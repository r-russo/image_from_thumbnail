import coloredlogs

from image_from_thumbnails import video_db


def main():
    coloredlogs.install()
    playlist_id = "UUAzKFALPuF_EPe-AEI0WFFw"
    videos = video_db.load_db(playlist_id)
    for v in videos[:10]:
        print(v.title, v.id)


if __name__ == "__main__":
    main()
