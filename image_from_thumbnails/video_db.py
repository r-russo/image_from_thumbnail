import logging
from dataclasses import dataclass
from pathlib import Path

from googleapiclient.discovery import build

from . import credentials


@dataclass(frozen=True)
class VideoInfo:
    id: str
    title: str


def load_db(
    playlist_id: str, fname: str = "db.txt", force_reload: bool = False
) -> list[VideoInfo]:
    f = Path(fname)

    if f.exists() and not force_reload:
        logging.info("File %s exists. Trying to load from file..." % fname)

        return _fetch_from_file(fname)

    videos = _fetch_channel(playlist_id)
    _save_to_file(fname, videos)
    return videos


def _fetch_channel(playlist_id: str, sort_oldest_first=True) -> list[VideoInfo]:
    videos: list[VideoInfo] = []
    logging.info("Connecting to Google API...")
    youtube = build("youtube", "v3", developerKey=credentials.google_api_key)
    logging.info("Getting videos list...")
    req = youtube.playlistItems().list(part="snippet", playlistId=playlist_id)
    logging.info("Fetching videos database...")
    while req is not None:
        response = req.execute()
        for i in response["items"]:
            title = i["snippet"]["title"]
            video_id = i["snippet"]["resourceId"]["videoId"]
            videos.append(VideoInfo(video_id, title))

        req = youtube.playlistItems().list_next(req, response)

    logging.info("Found %d videos in playlist", len(videos))

    req = youtube.channels.list()

    if sort_oldest_first:
        return videos[::-1]

    return videos


def _fetch_from_file(fname: str) -> list[VideoInfo]:
    with open(fname, "r") as f:
        lines = f.read().splitlines()

    videos: list[VideoInfo] = []
    for line in lines:
        id, title = line.split(";;")
        videos.append(VideoInfo(id, title))

    return videos


def _save_to_file(fname: str, videos: list[VideoInfo]):
    with open(fname, "w") as f:
        for video in videos:
            f.write(f"{video.id};;{video.title}\n")
